using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {

        private static List<Venda> vendas = new List<Venda>();

        [HttpPost]
        public IActionResult registrarVenda(Venda venda)
        {
            if(venda.Itens.Count < 1)
            {
                  return BadRequest(new { Erro = "A venda deve conter pelo menos 1 item!" });
            }
            vendas.Add(venda);

            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult buscarVendaById(int id)
        {
            bool contem = false;
            Venda venda = null;
            for (int i = 0; i < vendas.Count; i++)
            {
                if(id == vendas[i].Id)
                {
                    contem = true;
                    venda = vendas[i];
                }
            }

            if(contem)
                return Ok(venda);
            else
                return NotFound();
        }

        [HttpPut]
        public IActionResult atualizarVenda(Venda venda)
        {
           for (int i = 0; i < vendas.Count; i++)
            {
                if(venda.Id == vendas[i].Id)
                {
                    switch(venda.Status)
                    {
                    case "Pagamento Aprovado":
                        if(vendas[i].Status == "Aguardando Pagamento")
                        {
                            vendas[i].Status = venda.Status;
                        }else{
                            throw new Exception();
                        }
                        break;
                    case "Enviado para transportadora":
                        if(vendas[i].Status == "Pagamento Aprovado")
                        {
                            vendas[i].Status = venda.Status;
                        }else{
                            throw new Exception();
                        }
                        break;
                    case "Entregue":
                        if(vendas[i].Status == "Enviado para transportadora")
                        {
                            vendas[i].Status = venda.Status;
                        }else{
                            throw new Exception();
                        }
                        break;
                    case "Cancelada":
                        if(vendas[i].Status == "Aguardando Pagamento" || vendas[i].Status == "Pagamento Aprovado")
                        {
                            vendas[i].Status = venda.Status;
                        }else{
                            throw new Exception();
                        }
                        break;
                    }
                    
                }
            }
            
            
            return Ok(venda);
        }
    }
}