using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Item
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }

        public Item()
        {
            
        }
    }
}