using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public long Id { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime Data { get; set; }
        public List<Item> Itens { get; set; }
        public string Status { get; set; }

        public Venda()
        {
            
        }
    }
}